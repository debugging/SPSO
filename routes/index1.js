var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res) {
	var msg = "error"
	var sess = req.session
	if (!req.session.username) {
		msg = "error no username"
	} else if (req.query.data+"|" == "-1|") {
		sess.counter = 1;
		msg = 1;
	} else {
		if (sess.counter == req.query.data) {
			sess.counter++;
			msg = sess.counter ;
		} else {
			msg = "error" + sess.counter + "," + req.query.data;
		}
	}
	res.send("msg:" + msg);
});


module.exports = router;